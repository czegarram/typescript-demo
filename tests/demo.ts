import { expect } from 'chai';
import { Demo } from '../src/demo'

describe('demo class', () => { // the tests container
    it('checking demo type', () => { // the single test
        let demo: Demo = new Demo();
        expect(demo.getType()).to.equals('test');
    });
});